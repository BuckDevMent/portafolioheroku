# PortafolioHeroku

Proyectos soportados por heroku testeados para conocer la tecnologia e implementarla en el proyecto general.
Los proyectos estan redireccionados a dominios gratuitos mediante cloudflare cnames para testear el cambio de dominio a freenom.


#NodeJS

https://turboknowledge.tk/

#PHP

https://machinlearning.ml/

#Python

https://customforces.cf/

#Scala-Play

https://goodadvanced.ga/

#Clojure

https://goodquantics.gq/

#NoLocalDev 

https://opofree.tk/

#MIX - Ghost-HerokuOneClickDeploy

https://goldquery.herokuapp.com/

#MIX - Keystone-YeomanGeneratorMongoDBAtlas

https://learntertaiment.ml


